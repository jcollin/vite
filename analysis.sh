#!/usr/bin/env bash
###
#
#  @file analysis.sh
#  @copyright 2008-2022 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @version 1.2.0
#  @author Mathieu Faverge
#  @date 2022-02-08
#
###

# Performs an analysis of Vite source code:
# - we consider to be in Vite's source code root
# - we consider having the coverage file vite.lcov in the root directory
# - we consider having cppcheck, rats, sonar-scanner programs available in the environment

# filter sources:
# - consider generated files in ${BUILDDIR}

if [ $# -gt 0 ]
then
    BUILDDIR=$1
fi
BUILDDIR=${BUILDDIR:-build}

./.gitlab-ci-filelist.sh $BUILDDIR

# Undefine this because not relevant in our configuration
#export UNDEFINITIONS="-UWIN32 -UWIN64 -U_MSC_EXTENSIONS -U_MSC_VER -U__SUNPRO_C -U__SUNPRO_CC -U__sun -Usun -U__cplusplus"

# run cppcheck analysis
cppcheck -v -f --language=c++ --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingInclude ${UNDEFINITIONS} --file-list=./filelist-c.txt 2> vite-cppcheck.xml

# run rats analysis
rats -w 3 --xml  `cat filelist.txt` > vite-rats.xml

# Set the default for the project key
SONARQUBE_PROJECTKEY=${SONARQUBE_PROJECTKEY:-hiepacs:vite:gitlab:master}

# create the sonarqube config file
cat > sonar-project.properties << EOF
sonar.host.url=https://sonarqube.inria.fr/sonarqube
sonar.login=$SONARQUBE_LOGIN

sonar.links.homepage=$CI_PROJECT_URL
sonar.links.scm=$CI_REPOSITORY_URL
sonar.links.ci=$CI_PROJECT_URL/pipelines
sonar.links.issue=$CI_PROJECT_URL/issues

sonar.projectKey=$SONARQUBE_PROJECTKEY
sonar.projectDescription=Parallel Sparse direct Solver
sonar.projectVersion=master

sonar.language=c++
sonar.sources=src, tests, plugins
sonar.inclusions=`cat filelist.txt | xargs echo | sed 's/ /, /g'`
sonar.sourceEncoding=UTF-8
sonar.cxx.errorRecoveryEnabled=true
sonar.cxx.compiler.charset=UTF-8
sonar.cxx.compiler.parser=GCC
sonar.cxx.compiler.regex=^(.*):(\\d+):\\d+: warning: (.*)\\[(.*)\\]$
sonar.cxx.compiler.reportPath=vite-build.log
#sonar.cxx.coverage.reportPath=vite-coverage.xml
sonar.cxx.cppcheck.reportPath=vite-cppcheck.xml
sonar.cxx.rats.reportPath=vite-rats.xml
sonar.cxx.jsonCompilationDatabase=build/compile_commands.json
EOF

# run sonar analysis + publish on sonarqube-dev
sonar-scanner -X > sonar.log
