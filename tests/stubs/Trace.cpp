/*
 ** This file is part of the ViTE project.
 **
 ** This software is governed by the CeCILL-A license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 **
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 **
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-A license and that you accept its terms.
 **
 **
 ** ViTE developpers are (for version 0.* to 1.0):
 **
 **        - COULOMB Kevin
 **        - FAVERGE Mathieu
 **        - JAZEIX Johnny
 **        - LAGRASSE Olivier
 **        - MARCOUEILLE Jule
 **        - NOISETTE Pascal
 **        - REDONDY Arthur
 **        - VUCHENER Clément
 **
 */

/*!
 *
 * \file Trace.hpp
 * \author  NOISETTE
 * \brief Bouchon
 *
 */
#include "Trace.hpp"

using namespace std;


Trace::Trace(){

}
Trace::~Trace(){
    free_str();
}

string Trace::to_string(std::map<std::string, Value *> extra_fields){
    string  *res =new string( "option()");/*
                                           if (extra_fields.size()>0){
                                           *res = "option(" ;
                                           for (unsigned int i=0;i<extra_fields.size();i++)
                                           (*res).append(extra_fields[i]->to_string() + " ");
                                           (*res).append(")");
                                           }
                                           else{
                                           (*res).assign("\0");
                                           }
                                           */
    to_freed.push_back(res);
    return *res;
}


void Trace::free_str(){
    for (unsigned int i = 0 ; i < to_freed.size() ; i ++)
        delete to_freed[i];

    if (!to_freed.empty())
        to_freed.clear();

}

void Trace::define_container_type(Name &alias, ContainerType *container_type_parent, std::map<std::string, Value *> extra_fields){
    cout << "define_container_type "
         << alias.to_string()
         << " "
         << *container_type_parent
         << " "
         << to_string(extra_fields)
         << endl;

    free_str();
}



void Trace::create_container(Date time, Name alias, ContainerType *type, Container *parent, map<string, Value *> extra_fields){
    cout << "create_container "
         << time.to_string()
         << " "
         << alias.to_string()
         << " "
         << *type
         << " "
         << *parent
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}



void Trace::destroy_container(Date time, Container *cont, ContainerType *type, map<string, Value *> extra_fields){
    cout << "destroy_container "
         << time.to_string()
         << " "
         << *cont
         << " "
         << *type
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}


void Trace::define_event_type(Name alias, ContainerType *container_type, map<string, Value *> extra_fields){
    cout << "define_event_type "
         << alias.to_string()
         << " "
         << *container_type
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}



void Trace::define_state_type(Name alias, ContainerType *container_type, map<string, Value *> extra_fields){
    cout << "define_state_type "
         << alias.to_string()
         << " "
         << *container_type
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}


void Trace::define_variable_type(Name alias, ContainerType *container_type, map<string, Value *> extra_fields){
    cout << "define_variable_type "
         << alias.to_string()
         << " "
         << *container_type
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}



void Trace::define_link_type(Name alias, ContainerType *ancestor, ContainerType *source, ContainerType *destination, map<string, Value *> extra_fields){
    cout << "define_link_type "
         << alias.to_string()
         << " "
         << *ancestor
         << " "
         << *source
         << " "
         << *destination
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}



void Trace::define_entity_value(Name alias, EntityType *entity_type, map<string, Value *> extra_fields){
    cout << "define_entity_value "
         << alias.to_string()
         << " "
         << *entity_type
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}



void Trace::set_state(Date time, StateType *type, Container *container, EntityValue *value, std::map<std::string, Value *> extra_fields){
    cout << "set_state "
         << time.to_string()
         << " "
         << *type
         << " "
         << *container
         << " "
         << *value
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}



void Trace::push_state(Date time, StateType *type, Container *container, EntityValue *value, map<string, Value *> extra_fields){
    cout << "push_state "
         << time.to_string()
         << " "
         << *type
         << " "
         << *container
         << " "
         << *value
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}



void Trace::pop_state(Date time, StateType *type, Container *container, map<string, Value *> extra_fields){

    cout << "pop_state "
         << time.to_string()
         << " "
         << *type
         << " "
         << *container
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}



void Trace::new_event(Date time, EventType *type, Container *container, EntityValue *value, map<string, Value *> extra_fields){
    cout << "new_event "
         << time.to_string()
         << " "
         << *type
         << " "
         << *container
         << " "
         << *value
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}



void Trace::set_variable(Date time, VariableType *type, Container *container, Double value, map<string, Value *> extra_fields){

    cout << "set_variable "
         << time.to_string()
         << " "
         << *type
         << " "
         << *container
         << " "
         << value.to_string()
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}


void Trace::add_variable(Date time, VariableType *type, Container *container, Double value, map<string, Value *> extra_fields){

    cout << "add_variable "
         << time.to_string()
         << " "
         << *type
         << " "
         << *container
         << " "
         << value.to_string()
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}



void Trace::sub_variable(Date time, VariableType *type, Container *container, Double value, map<string, Value *> extra_fields){

    cout << "sub_variable "
         << time.to_string()
         << " "
         << *type
         << " "
         << *container
         << " "
         << value.to_string()
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}


void Trace::start_link(Date &time, LinkType *type, Container *ancestor, Container *source, EntityValue *value, String key, std::map<std::string, Value *> extra_fields){

    cout << "start_link "
         << time.to_string()
         << " "
         << *type
         << " "
         << *ancestor
         << " "
         << *source
         << " "
         << *value
         << " "
         << key.to_string()
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();

}



void Trace::end_link(Date &time, LinkType *type, Container *ancestor, Container *destination, EntityValue *value, String key, std::map<std::string, Value *> extra_fields){

    cout << "end_link "
         << time.to_string()
         << " "
         << *type
         << " "
         << *ancestor
         << " "
         << *destination
         << " "
         << *value
         << " "
         << key.to_string()
         << " "
         << to_string(extra_fields)
         << endl;
    free_str();
}





Container *Trace::search_container_type(String name) const{
    string *res = new string((string)"search(" + name.to_string() + ")");
    to_freed.push_back(res);
    return res;
}


Container *Trace::search_container(String name) const{
    string* res = new string((string)"search(" + name.to_string() + ")");
    to_freed.push_back(res);
    return res;
}

Container *Trace::search_container(string name) const{
    string* res = new string((string)"search(" + name + ")");
    to_freed.push_back(res);
    return res;
}


Container *Trace::search_event_type(String name){
    string* res = new string((string)"search(" + name.to_string() + ")");
    to_freed.push_back(res);
    return res;
}



Container *Trace::search_state_type(String name){
    string* res = new string("search(" + name.to_string() + ")");
    to_freed.push_back(res);
    return res;
}



VariableType *Trace::search_variable_type(String name){
    string* res = new string("search(" + name.to_string() + ")");
    to_freed.push_back(res);
    return res;
}



LinkType *Trace::search_link_type(String name){
    string* res = new string((string)"search(" + name.to_string() + ")");
    to_freed.push_back(res);
    return res;
}



EntityValue *Trace::search_entity_value(String name, EntityType *entity_type) const{
    string* res = new string((string)"search(" + name.to_string() + ")");
    to_freed.push_back(res);
    return res;
}

EntityType *Trace::search_entity_type(String name) const{
    string* res = new string((string)"search(" + name.to_string() + ")");
    to_freed.push_back(res);
    return res;
}

void Trace::finish(){
    cout << "finish" << endl;
}
