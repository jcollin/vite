/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/
/*! 
 * \mainpage ViTE 
 *
 ViTE is a trace explorer. It is a tool to visualize execution traces in Pajé format for debugging and profiling parallel or distributed applications.
 * \image html logo.png
 * \image latex logo.png
 *
 * \authors COULOMB Kevin
 * \authors FAVERGE Mathieu
 * \authors JAZEIX Johnny
 * \authors LAGRASSE Olivier
 * \authors MARCOUEILLE Jule
 * \authors NOISETTE Pascal
 * \authors REDONDY Arthur
 * \authors VUCHENER Clément
 *
 * \file main.cpp
 * \brief The main launcher.
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <map>
#include <list>

/* Global informations */
#include "common/common.hpp"
#ifdef MEMORY_TRACE
#include "common/TraceMemory.hpp"
#endif
/* -- */
#include "common/Memory.hpp"
#include "common/Tools.hpp"
/* -- */
#include "otf.h"
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
#include "parser/Parser.hpp"
#include "parser/ParserOTF.hpp"

#define MEMORY_WRITE(mem)      ((mem < 1<<10)?mem:((mem < 1<<20 )?mem/(1<<10):((mem < 1<<30 )?(double)mem/(double)(1<<20):(double)mem/(double)(1<<30))))
#define MEMORY_UNIT_WRITE(mem) ((mem < 1<<10)?"o":((mem < 1<<20 )?"Ko":((mem < 1<<30 )?"Mo":"Go")))

/*!
 *\brief The main function of ViTE.
 */

int main(int argc, char **argv) {
    Parser         *parser;
    Trace          *mytrace;
#ifdef MEMORY_TRACE
    FILE           *file;
#endif
    double          timestamp = 0.0;
    
    /* Get the start time */
    timestamp = clockGet();

#ifdef MEMORY_TRACE
    file      = fopen("toto.trace", "w");
    memAllocTrace(file, timestamp, 0);
    trace_start(file, timestamp, 0, -1);
#endif

    /* Initialize Parser handler */
    parser = new ParserOTF("test.otf");

    /* New trace handler */
    mytrace = new Trace();
        
    /* Informations beforge parsing */
    fprintf(stdout, "Before Parse \n"
	    "Memory allocated     : %.3g %s\n", 
	    MEMORY_WRITE(memAllocGetCurrent()), 
	    MEMORY_UNIT_WRITE(memAllocGetCurrent()));

    /* Parse the file */
    parser->parse(*mytrace);
 
    /* Informations after parsing */
    fprintf(stdout, "After parse \n"
	    "Time to parse file   : %.3g s \n"
	    "Max Memory allocated : %.3g %s\n" 
	    "Memory allocated     : %.3g %s\n", 
	    (clockGet()-timestamp), 
	    MEMORY_WRITE(memAllocGetMax()),     MEMORY_UNIT_WRITE(memAllocGetMax()),
	    MEMORY_WRITE(memAllocGetCurrent()), MEMORY_UNIT_WRITE(memAllocGetCurrent()));

    delete parser;       

    fprintf(stdout, "After deleting parser and thread \n"
	    "Max Memory allocated : %.3g %s\n"
	    "Memory allocated     : %.3g %s\n", 
	    MEMORY_WRITE(memAllocGetMax()),     MEMORY_UNIT_WRITE(memAllocGetMax()),
	    MEMORY_WRITE(memAllocGetCurrent()), MEMORY_UNIT_WRITE(memAllocGetCurrent()));

    delete mytrace;

    fprintf(stdout, "After deleting trace\n"
	    "Max Memory allocated : %.3g %s\n"
	    "Memory allocated     : %.3g %s\n", 
	    MEMORY_WRITE(memAllocGetMax()),     MEMORY_UNIT_WRITE(memAllocGetMax()), 
	    MEMORY_WRITE(memAllocGetCurrent()), MEMORY_UNIT_WRITE(memAllocGetCurrent()));

#ifdef MEMORY_TRACE
    trace_finish(file, (clockGet()-timestamp), 0, -1);
    memAllocUntrace();
#endif

    return EXIT_SUCCESS;
}
