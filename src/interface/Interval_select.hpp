/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 *\file Interval_select.hpp
 */

#ifndef INTERVAL_SELECT_HPP
#define INTERVAL_SELECT_HPP

class Interval_select;

/* For moc compilation */
#include <string>
#include <map>
#include <list>
/* -- */
#include <QWidget>
#include "ui_interval_select.h"

#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/tree/Interval.hpp"
#include "trace/Trace.hpp"

#include "interface/Interface_graphic.hpp"

/* -- */

/*!
 * \class Node select
 * \brief Class used to select which containers should be displayed
 *
 */

class Interval_select : public QDialog, protected Ui::interval_selector
{

    Q_OBJECT
    friend class Interface_graphic;

private:
    Trace *_trace;
    Interface_graphic *_console;
    bool _applied;
    bool _auto_refresh;

public:
    /*!
     * Default constructor
     * \param parent The parent widget of the window.
     */
    Interval_select(Interface_graphic *console, QWidget *parent = nullptr);

    ~Interval_select() override;

    /*!
     * \fn set_trace(Trace *trace)
     * \brief Set the trace parsed (give the container names)
     * \param trace The trace.
     */
    void set_trace(Trace *trace);

    /*!
     * \fn get_trace()
     * \brief returns the trace
     */
    Trace *get_trace();

    void apply_settings();
    void update_values();

private Q_SLOTS:
    void minSpinBoxValueChanged(double value);
    void minSliderValueChanged(int value);
    void maxSpinBoxValueChanged(double value);
    void maxSliderValueChanged(int value);

    void on_ok_button_clicked();
    void on_cancel_button_clicked();
    void on_reset_button_clicked();
    void on_apply_button_clicked();
    void on_auto_refresh_box_stateChanged();

Q_SIGNALS:
    void minValueChanged(double value);
    void maxValueChanged(double value);
};

#endif // INTERVAL_SELECT_HPP
