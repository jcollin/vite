/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 *\file Settings_window.hpp
 */

#ifndef SETTINGS_WINDOW_HPP
#define SETTINGS_WINDOW_HPP

/* For moc compilation */
#include <QWidget>
#include <QTableWidget>
#include <QColor>
#include "ui_settings.h"

class Core;
class Trace;
class Color;
class QtColorPicker;
class QCheckBox;
class EntityValue;

/*!
 * \class Settings_window
 * \brief Class used to display statistics of containers.
 *
 */

class Settings_tab : public QWidget
{
    Q_OBJECT

public:
    QVBoxLayout *background;

    QHBoxLayout *header;
    QLabel *hdr_label;
    QComboBox *list_palette;
    QSpacerItem *hdr_spacer;
    QPushButton *btn_palette_cp;
    QPushButton *btn_palette_rm;

    QTableWidget *content;

    QHBoxLayout *footer;
    QSpacerItem *ftr_spacer;
    QPushButton *btn_apply;
    QPushButton *btn_cancel;
    QPushButton *btn_reload;

    Core *_core;
    std::string _ecname;
    // EntityClass_t _ectype;

    bool _changed;
    std::map<std::string, int> _changes;
    std::map<std::string, int> _cancel;

    Settings_tab(Core *, std::string);
    ~Settings_tab() override;

    void refresh();

private:
    void add_table_line(int &row,
                        EntityValue *ev,
                        bool used);
    void fill_table(bool used = true);
    void update_table_from_palette(const std::string &);

public Q_SLOTS:
    void on_btn_apply_clicked();
    void on_btn_cancel_clicked();

private Q_SLOTS:

    /*!
     * \fn item_changed(int row = -1)
     * \brief Slot called when the user changes an object color, or the
     *        visibility in the table.
     */
    void item_changed(int row = -1);

    void on_list_palette_currentIndexChanged(const QString &text);
    void on_btn_palette_cp_clicked();
    void on_btn_palette_rm_clicked();
    void on_btn_reload_clicked();
};

class Settings_window : public QWidget, protected Ui::settings
{

    Q_OBJECT
    friend class Interface_graphic;

private:
    Core *_c;
    Settings_tab *_tab_states;
    Settings_tab *_tab_events;
    Settings_tab *_tab_links;

    bool _changed;

    /*!
     * \brief Load in the QListWidget the directory name from the Session
     */
    void plugin_load_dirs_name();

    /*!
     * \brief Reload the general tab.
     */
    void reload_general_tab();

    /*!
     * \brief Reload the minimap tab.
     */
    void reload_minimap_tab();

public:
    /*!
     * Default constructor
     * \param parent The parent widget of the window.
     */
    Settings_window(Core *c, QWidget *parent = nullptr);

    ~Settings_window() override;

    /*!
     * \brief Overloaded to store at beginning the values
     * (to get them back if we cancel for example)
     */
    void show();
    void refresh();

public Q_SLOTS:
    /*!
     * \fn on_tabWidget_currentChanged(int index)
     * \brief Called when the user changes the tab.
     * \param index : the index of the new tab.
     * Initialize the new tab if needed.
     */
    void on_tabWidget_currentChanged(int index);

private Q_SLOTS:

    /*!
     * \fn on_rm_dir_btn_clicked()
     * \brief (plugin tab) Called when the user click on the remove directory button.
     * Remove the directory from the list where we look for the plugins.
     */
    void on_rm_dir_btn_clicked();

    /*!
     * \fn on_add_dir_btn_clicked()
     * \brief (plugin tab) Called when the user click on the add directory button.
     * Add the directory from the list where we look for the plugins.
     */
    void on_add_dir_btn_clicked();

    /*!
     * \fn on_cancel_clicked()
     * \brief (Global) Called when the user click on the cancel button.
     * Quit the window without saving changes.
     */
    void on_btn_cancel_clicked();

    /*!
     * \fn on_ok_clicked()
     * \brief (states tab) Called when the user click on the OK button.
     * apply changes and hide the window
     */
    void on_btn_ok_clicked();

    /*!
     *\brief A slot called when 'arrows_shape' in the menu is modified
     */
    void on_cb_nolinks_stateChanged(int state);
    void on_cb_noevents_stateChanged(int state);
    void on_cb_tip_currentIndexChanged(int index);

Q_SIGNALS:
    void settings_changed();
};

#endif // SETTINGS_WINDOW_HPP
