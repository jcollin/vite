/*
 ** This file is part of the ViTE project.
 **
 ** This software is governed by the CeCILL-A license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 **
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 **
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-A license and that you accept its terms.
 **
 **
 ** ViTE developers are (for version 0.* to 1.0):
 **
 **        - COULOMB Kevin
 **        - FAVERGE Mathieu
 **        - JAZEIX Johnny
 **        - LAGRASSE Olivier
 **        - MARCOUEILLE Jule
 **        - NOISETTE Pascal
 **        - REDONDY Arthur
 **        - VUCHENER Clément
 **
 */
/*!
 *\file Interface.hpp
 */

#ifndef INTERFACE_HPP
#define INTERFACE_HPP

class Interface;

/*!
 *\brief  This is an interface, used by the terminal and graphical interfaces.
 *
 *Interface defines functions implemented in their inherited classes. It gives functions which can be used by others parts of ViTE (for example the Parser and the Data Structure). Thus, it hides which kind of interface is used: a console interface (where messages are displayed in the terminal) or a graphical interface (where messages are displayed in a dialog box).
 */
class Interface
{

public:
    /*!
     * \arg string : the string to be displayed.
     * \brief The function takes a string then displayed it either on the terminal if there is an Interface_console instance, or on a dialog box for the Interface_graphic. Then, it killed the application.
     */
    virtual void error(const std::string &) const = 0;

    /*!
     \arg string : the string to be displayed.
     \brief The function takes a string then displayed it either on the terminal if there is an Interface_console instance, or on a dialog box for the Interface_graphic. Then the program go on, but with an indeterminated behaviour.
     */
    virtual void warning(const std::string &) const = 0;

    /*!
     \arg string : the string to be displayed.
     \brief The function takes a string then displayed it either on the terminal if there is an Interface_console instance, or on a dialog box for the Interface_graphic. Then the program go on normally.
     */
    virtual void information(const std::string &) const = 0;

    /*!
     *\arg string : the string to be displayed.
     *\brief The function takes strings and/or numbers then displayed it in the entity informative text area in the info window (use in graphic interface).
     */
    virtual void selection_information(const std::string &) const = 0;

    /*!
     \brief Return the name of the file opened.
     */
    virtual const std::string get_filename() const = 0;
};

#endif
