#ifndef VITETREEWIDGET_H
#define VITETREEWIDGET_H

#include <QTreeWidget>
#include <QObject>

class viteQTreeWidget : public QTreeWidget
{
    Q_OBJECT

public:
    viteQTreeWidget(QWidget *parent = nullptr);
    ~viteQTreeWidget() override;

protected:
    void dropEvent(QDropEvent *event) override;

    void dragEnterEvent(QDragEnterEvent *e) override;
    void dragMoveEvent(QDragMoveEvent *e) override;

    QList<QTreeWidgetItem *> selected_items;

#if QT_VERSION >= 0x060000
    QMimeData *mimeData(const QList<QTreeWidgetItem *> &items) const override;
#else
    QMimeData *mimeData(const QList<QTreeWidgetItem *> items) const override;
#endif

    bool dropMimeData(QTreeWidgetItem *newParentPtr, int index, const QMimeData *data, Qt::DropAction action) override;

    Qt::DropActions supportedDropActions() const override;
};

#endif
