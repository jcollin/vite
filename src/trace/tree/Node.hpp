/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef NODE_HPP
#define NODE_HPP

/*!
 * \file BinaryTree.hpp
 */
template <typename E>
class BinaryTree;
/*!
 * \class Node
 * \brief Describe a node in a binary tree bound to a element
 */
template <typename E>
class Node
{
    friend class BinaryTree<E>;

private:
    Node<E> *_parent;
    Node<E> *_left_child, *_right_child;

    E *_element;

public:
    /*!
     * \brief Undisplayble interval at the left
     */
    Interval *_left_interval;

    /*!
     * \brief Undisplayble interval at the right
     */
    Interval *_right_interval;

    /*!
     * \brief Constructor
     * \param element Element bound to the node
     */
    Node(E *element) :
        _parent(nullptr), _left_child(nullptr), _right_child(nullptr), _element(element), _left_interval(nullptr), _right_interval(nullptr) {
    }

    /*!
     * \brief Destructor
     */
    ~Node() {
        if (_left_child)
            delete _left_child;
        if (_right_child)
            delete _right_child;
        // delete _element;
        _element = NULL;
        _left_child = NULL;
        _right_child = NULL;
        _parent = NULL;
    }

    /*!
     * \brief Get the parent of the node
     */
    Node<E> *get_parent() const {
        return _parent;
    }

    /*!
     * \brief Get the left child of the node
     */
    Node<E> *get_left_child() const {
        return _left_child;
    }

    /*!
     * \brief Get the right child of the node
     */
    Node<E> *get_right_child() const {
        return _right_child;
    }

    /*!
     * \brief Get the element bound to the node
     */
    const E *get_element() const {
        return _element;
    }
};

#endif
