/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/

#ifndef EVTBUILDER_HPP
#define EVTBUILDER_HPP

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QSemaphore>
#include <queue>
#include <trace/IntervalOfContainer.hpp>
#include <trace/Serializer.hpp>

/*!
 * \file SerializerWriter.hpp
 * Thread that handles the serialization of an IntervalOfContainer
 */

class SerializerWriter : public QObject, public Singleton<SerializerWriter>
{
    Q_OBJECT

private:
    QWaitCondition *_cond;
    QSemaphore *_freeSlots;
    QSemaphore *_itcProduced;
    QThread *_thread;
    QMutex *_mutex;
    bool _is_finished;

public:
    SerializerWriter();

    SerializerWriter(QWaitCondition *cond, QSemaphore *freeSlots, QSemaphore *itcProduced, QThread *thread, QMutex *mutex);

    // void set_values(QWaitCondition* cond, QSemaphore* free, QSemaphore* itc,QThread* thread);

    QWaitCondition *get_cond();

    QSemaphore *get_sem_free();

    QSemaphore *get_sem_produced();

    QThread *get_thread();

public Q_SLOTS:

    void dump_on_disk(IntervalOfContainer *itc, char *filename);

    void load(IntervalOfContainer *itc, char *filename);

    bool is_finished();

    void finish_build();
};

#endif
