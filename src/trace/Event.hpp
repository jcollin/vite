/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef EVENT_HPP
#define EVENT_HPP

/*!
 * \file Event.hpp
 */

class Event;
template <typename E>
class BinaryTree;

/*!
 * \class Event
 * \brief Describe an event
 */
class Event : public Entity
{
public:
    typedef std::list<Event *> Vector;
    typedef std::list<Event *>::const_iterator VectorIt;
    typedef BinaryTree<Event> Tree;

private:
    Date _time;
    EventType *_type;
    EntityValue *_value;

public:
    /*!
     * \fn Event(Date time, EventType *type, Container *container, EntityValue *value, std::map<std::string, Value *> opt)
     * \brief Event Constructor
     * \param time Date of the event
     * \param type Type of the event
     * \param container Container of the event
     * \param value Value of the event
     * \param opt : optional data in the event
     */
    Event(Date time, EventType *type, Container *container, EntityValue *value, std::map<std::string, Value *> opt);

    /*!
     * \fn get_time() const
     * \brief Get the time of the event
     */
    Date get_time() const;

    /*!
     * \fn get_type() const
     * \brief Get the type of the event
     */
    const EventType *get_type() const;

    /*!
     * \fn get_value() const
     * \brief Get the value of the event
     * \return Pointer to the Entityvalue or NULL if it has no value
     */
    EntityValue *get_value() const;
};

#endif
