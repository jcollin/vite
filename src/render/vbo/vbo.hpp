/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developpers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 *\file vbo.hpp
 */

#ifndef VBO_HPP
#define VBO_HPP
#include <vector>
#include "common/common.hpp"
using namespace std;

// macro used by OpenGL
#define BUFFER_OFFSET(a) ((char *)NULL + (a))
class Shader;
/*!
 * \brief Manage the Vertex Buffer Object.
 */
class Vbo
{

private:
    GLuint _vboID;
    GLuint _vaoID;
    Shader *_shader;
    vector<Element_pos> _vertex;
    vector<Element_col> _colors;
    vector<char> _shaded;
    vector<float> _shaded2;
    vector<Element_pos> _texture_coord;
    int _nbVertex;

public:
    /***********************************
     *
     * Constructor and destructor.
     *
     **********************************/

    /*!
     * \brief Constructor.
     */
    Vbo();

    /*!
     * \brief Constructor.
     * \param Shader the shader we use for the entity type the vbo stand for
     */
    Vbo(Shader *s);

    /*!
     * \brief The destructor.
     */
    virtual ~Vbo();

    /***********************************
     *
     * Buffer filling.
     *
     **********************************/

    /*!
     * \brief Add vertex to a vbo storing coordinates and colors
     */
    int add(Element_pos x, Element_pos y, Element_col r, Element_col g, Element_col b);
    /*!
     * \brief Add vertex to a vbo storing coordinates and textures
     */
    int add(Element_pos x, Element_pos y, Element_pos tx, Element_pos ty);
    /*!
     * \brief Add vertex to a state vbo using char for color gradient (glsl >= 330)
     */
    int add(Element_pos x, Element_pos y, char b);
    /*!
     * \brief Add vertex to a state vbo using float for color gradient (glsl < 330)
     */
    int add(Element_pos x, Element_pos y, float b);
    /*!
     \brief Add vertex to a link/event vbo
     */
    int add(Element_pos x, Element_pos y);
    /*!
     * \brief Should be called before using vbo in paintGL
     */
    void lock();
    /*!
     * \brief Should be called after using vbo in paintGL
     */
    void unlock();
    int getNbVertex();
    void setNbVertex(int);
    /*!
     * \brief Configuration function. Create VBO and VAO and send datas to GPU
     */
    void config(int glsl);
    Shader *get_shader();
    /*
     * \brief delete current used shader
     */
    void delete_shader();
    /*
     * \brief Change Shader used with this vbo
     */
    void set_shader(Shader *s);
};

#endif
