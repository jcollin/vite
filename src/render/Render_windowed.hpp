/*
 ** This file is part of the ViTE project.
 **
 ** This software is governed by the CeCILL-A license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 **
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 **
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-A license and that you accept its terms.
 **
 **
 ** ViTE developers are (for version 0.* to 1.0):
 **
 **        - COULOMB Kevin
 **        - FAVERGE Mathieu
 **        - JAZEIX Johnny
 **        - LAGRASSE Olivier
 **        - MARCOUEILLE Jule
 **        - NOISETTE Pascal
 **        - REDONDY Arthur
 **        - VUCHENER Clément
 **
 */

#ifndef VITE_RENDER_WINDOWED_HPP
#define VITE_RENDER_WINDOWED_HPP

#include "Render.hpp"

/**
 * \brief Allow to render on a QT window
 */
class Render_windowed : public Render
{

public:
    /**
     * \brief Get the widget associated to the renderer to display in the window
     * @return
     */
    virtual QWidget *get_render_widget() = 0;

    /**
     * Get the last frame displayed and save it on a QT image
     * @return
     */
    virtual QImage grab_frame_buffer() = 0;

    /*!
     * \brief This function draws the trace.
     */
    virtual bool build() = 0;

    /*!
     * \brief This function releases the trace.
     */
    virtual bool unbuild() = 0;

    virtual void release() = 0;

    virtual void show_minimap() = 0;

    /*!
     * \brief Refresh scroll bar positions when shortcuts execute movements
     * \param LENGTH_CHANGED If true the total length of the scroll bar will be updated.
     */
    virtual void refresh_scroll_bars(bool LENGTH_CHANGED = false) = 0;

    virtual void apply_zoom_on_interval(Element_pos t1, Element_pos t2) = 0;

    /*!
     * \brief Change the x position of camera view for state drawing area.
     * \param translate The new position.
     */
    virtual void change_translate(int translate) = 0;

    /*!
     * \brief Change the percentage taken by container display in the render area.
     * \param view_size The new percentage (between 0 to 100).
     */
    virtual void change_scale_container_state(int view_size) = 0;

    /*!
     * \brief Change the scale of the y state drawing.
     * \param scale_coeff The new coefficient scale value to add to the current scale.
     */
    virtual void change_scale_y(Element_pos scale_coeff) = 0;

    /*!
     * \brief Replace the current scale by a new scale. (horizontally)
     * \param new_scale The new scale value to replace the current scale.
     */
    virtual void replace_scale(Element_pos new_scale) = 0;

    /*!
     * \brief Replace the current y translate by a new translate.
     * \param new_translate The new translate value to replace the current translate.
     */
    virtual void replace_translate_y(Element_pos new_translate) = 0;

    /*!
     * \brief Pre registered translation values (for x or y translate).
     * \param id The pre registered translation id.
     */
    virtual void registered_translate(int id) = 0;

    /*!
     * \brief Change the scale of state drawing.
     * \param scale_coeff The new coefficient scale value to add to the current scale.
     */
    virtual void change_scale(Element_pos scale_coeff) = 0;

    /*!
     * \brief Replace the current scale by a new scale. (vertically)
     * \param new_scale The new scale value to replace the current scale.
     */
    virtual void replace_scale_y(Element_pos new_scale) = 0;

    /*!
     * \brief Replace the current x translate by a new translate.
     * \param new_translate The new translate value to replace the current translate.
     */
    virtual void replace_translate(Element_pos new_translate) = 0;

    /**
     * Checks if the render is valid
     * @return true if the render is valid or false otherwise
     */
    virtual bool is_validated() = 0;

    /**
     * \brief Updates the widget
     */
    virtual void update_render() = 0;
};

#endif // VITE_RENDER_WINDOWED_HPP
