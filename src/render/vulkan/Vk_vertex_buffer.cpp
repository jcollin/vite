/*
 ** This file is part of the ViTE project.
 **
 ** This software is governed by the CeCILL-A license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 **
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 **
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-A license and that you accept its terms.
 **
 **
 ** ViTE developers are (for version 0.* to 1.0):
 **
 **        - COULOMB Kevin
 **        - FAVERGE Mathieu
 **        - JAZEIX Johnny
 **        - LAGRASSE Olivier
 **        - MARCOUEILLE Jule
 **        - NOISETTE Pascal
 **        - REDONDY Arthur
 **        - VUCHENER Clément
 **
 */
#include "render/vulkan/Vk_vertex_buffer.hpp"

Vk_vertex_buffer::Vk_vertex_buffer::Vk_vertex_buffer() :
    Vk_buffer(VK_BUFFER_USAGE_VERTEX_BUFFER_BIT) { }

uint32_t Vk_vertex_buffer::vertex_count() {
    return _vertex_count;
}

void Vk_vertex_buffer::bind_vertex_buffer(VkCommandBuffer *command_buffer) {
    VkDeviceSize vb_offset = 0;
    _dev_funcs->vkCmdBindVertexBuffers(*command_buffer, 0, 1, &_buffer, &vb_offset);
}

void Vk_vertex_buffer::draw(VkCommandBuffer *command_buffer) {
    if (_vertex_count > 0) {
        _dev_funcs->vkCmdDraw(*command_buffer, _vertex_count, 1, 0, 0);
    }
}