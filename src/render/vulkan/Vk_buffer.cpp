/*
 ** This file is part of the ViTE project.
 **
 ** This software is governed by the CeCILL-A license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 **
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 **
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-A license and that you accept its terms.
 **
 **
 ** ViTE developers are (for version 0.* to 1.0):
 **
 **        - COULOMB Kevin
 **        - FAVERGE Mathieu
 **        - JAZEIX Johnny
 **        - LAGRASSE Olivier
 **        - MARCOUEILLE Jule
 **        - NOISETTE Pascal
 **        - REDONDY Arthur
 **        - VUCHENER Clément
 **
 */
#include "render/vulkan/Vk_buffer.hpp"

Vk_buffer::Vk_buffer(VkBufferUsageFlagBits flags) :
    _flags(flags) { }

Vk_buffer::~Vk_buffer() {
    free_buffer();
}

void Vk_buffer::init(VkDevice dev, QVulkanDeviceFunctions *dev_funcs, uint32_t memory_index) {
    _dev = dev;
    _dev_funcs = dev_funcs;
    _memory_index = memory_index;
}

void Vk_buffer::alloc_buffer(VkDeviceSize size) {
    if (size == 0)
        size = 1;
    // Initialize buffer information for its creation
    VkBufferCreateInfo buf_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = size,
        .usage = _flags
    };

    VkResult err = _dev_funcs->vkCreateBuffer(_dev, &buf_info, nullptr, &_buffer);
    if (err != VK_SUCCESS)
        qFatal("Failed to create buffer: %d", err);

    // Initialize the memory needed by the buffer
    VkMemoryRequirements mem_requirements;
    _dev_funcs->vkGetBufferMemoryRequirements(_dev, _buffer, &mem_requirements);

    // Allocate the memory needed
    VkMemoryAllocateInfo allocation_info = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext = nullptr,
        .allocationSize = mem_requirements.size,
        .memoryTypeIndex = _memory_index
    };

    err = _dev_funcs->vkAllocateMemory(_dev, &allocation_info, nullptr, &_buf_mem);
    if (err != VK_SUCCESS)
        qFatal("Failed to allocate memory: %d", err);

    // Bind the allocated memory to the buffer
    err = _dev_funcs->vkBindBufferMemory(_dev, _buffer, _buf_mem, 0);
    if (err != VK_SUCCESS)
        qFatal("Failed to bind buffer memory: %d", err);

    _mem_size = mem_requirements.size;
}

void Vk_buffer::free_buffer() {
    if (_buffer) {
        _dev_funcs->vkDestroyBuffer(_dev, _buffer, nullptr);
        _buffer = VK_NULL_HANDLE;
    }
    if (_buf_mem) {
        _dev_funcs->vkFreeMemory(_dev, _buf_mem, nullptr);
        _buf_mem = VK_NULL_HANDLE;
    }
}

void Vk_buffer::realloc_buffer(VkDeviceSize new_size) {
    free_buffer();
    alloc_buffer(new_size);
}

VkDeviceSize Vk_buffer::get_mem_size() {
    return _mem_size;
}