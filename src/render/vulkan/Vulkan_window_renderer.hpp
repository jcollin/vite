/*
 ** This file is part of the ViTE project.
 **
 ** This software is governed by the CeCILL-A license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 **
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 **
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-A license and that you accept its terms.
 **
 **
 ** ViTE developers are (for version 0.* to 1.0):
 **
 **        - BREDEL Nolan
 **        - GAUCHET Augustin
 **        - GUEDON Lucas
 **
 */

#ifndef VULKAN_WINDOW_RENDERER_HPP
#define VULKAN_WINDOW_RENDERER_HPP

#include <QVulkanWindow>
#include "common/common.hpp"
#include "core/Core.hpp"
#include "render/vulkan/Vk_vertex_buffer.hpp"
#include "render/vulkan/Vk_uniform_buffer.hpp"
#include "render/vulkan/Vk_pipeline.hpp"

/*!
 * \brief The internal representation of a vertex
 */
struct Vertex
{
public:
    Element_pos x, y;
    Element_col r, g, b;

    Vertex(Element_pos x, Element_pos y, Element_col r, Element_col g, Element_col b) :
        x(x), y(y), r(r), g(g), b(b) { }
};

class Vulkan_window_renderer : public QVulkanWindowRenderer
{
public:
    Vulkan_window_renderer(Core *core, QVulkanWindow *w);
    void initResources() override;
    void initSwapChainResources() override;
    void releaseSwapChainResources() override;
    void releaseResources() override;
    void startNextFrame() override;

protected:
    /*!
     * \brief Loads a compiled shader from the file with the given name
     */
    VkShaderModule create_shader(const QString &shader_name);
    /*!
     * \brief Creates the descriptor pool required for the rendering
     */
    void create_descriptor_pool();
    /*!
     * \brief Creates the pipeline required for the rendering
     */
    void create_pipeline();
    void draw_vertices(Vk_uniform_buffer *uniform_buf, Vk_vertex_buffer *vertex_buf);

    Core *_core;
    QVulkanWindow *_window;
    QVulkanDeviceFunctions *_dev_funcs;

    Vk_uniform_buffer _buf_container_uniform;
    Vk_uniform_buffer _buf_state_uniform;
    Vk_uniform_buffer _buf_static_uniform;

    VkDescriptorPool _desc_pool = VK_NULL_HANDLE;
    VkDescriptorSetLayout _desc_set_layout = VK_NULL_HANDLE;
    VkPipelineCache _pipeline_cache = VK_NULL_HANDLE;
    VkPipelineLayout _pipeline_layout = VK_NULL_HANDLE;
    Vk_pipeline _triangle_pipeline;
    Vk_pipeline _line_pipeline;

    /*!
     * \brief The current projection matrix, to addapt the view to the size of the window
     */
    QMatrix4x4 _proj;

public:
    /*!
     * \brief The current model matrix for rendering states, representing the translation and scale applied to the view
     */
    QMatrix4x4 state_model_view;
    /*!
     * \brief The current model matrix for rendering containers, representing the translation and scale applied to the view
     */
    QMatrix4x4 container_model_view;
    Vk_vertex_buffer buf_container_vertex;
    Vk_vertex_buffer buf_state_vertex;
    Vk_vertex_buffer buf_ruler;
    Vk_vertex_buffer buf_counter;
};

#endif
