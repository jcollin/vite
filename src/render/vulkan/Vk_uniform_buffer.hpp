/*
 ** This file is part of the ViTE project.
 **
 ** This software is governed by the CeCILL-A license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 **
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 **
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-A license and that you accept its terms.
 **
 **
 ** ViTE developers are (for version 0.* to 1.0):
 **
 **        - COULOMB Kevin
 **        - FAVERGE Mathieu
 **        - JAZEIX Johnny
 **        - LAGRASSE Olivier
 **        - MARCOUEILLE Jule
 **        - NOISETTE Pascal
 **        - REDONDY Arthur
 **        - VUCHENER Clément
 **
 */
#ifndef VK_UNIFORM_BUFFER_HPP
#define VK_UNIFORM_BUFFER_HPP

#include "render/vulkan/Vk_buffer.hpp"

class Vk_uniform_buffer : public Vk_buffer
{

private:
    VkDescriptorSet _desc_set[QVulkanWindow::MAX_CONCURRENT_FRAME_COUNT];
    VkDescriptorBufferInfo _uniform_buf_info[QVulkanWindow::MAX_CONCURRENT_FRAME_COUNT];

public:
    Vk_uniform_buffer();

    /*!
     * \brief Creates the buffer on the GPU with the given size and initialises the content of the buffer by initialising the uniforms with identity matrices
     */
    void init_uniform(const VkDescriptorSetAllocateInfo *descriptor_info, uint32_t concurrent_frame_count, VkDeviceSize alloc_size);

    void set_variable(unsigned int frame_index, VkDeviceSize offset, VkDeviceSize size, const void *value);

    void bind_uniform_buffer(VkCommandBuffer &command_buffer, uint32_t frame_index, VkPipelineLayout &pipeline_layout);
};

#endif