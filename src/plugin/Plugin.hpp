/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/

#ifndef PLUGIN_HPP
#define PLUGIN_HPP

class Trace;
class QVariant;
class Render_windowed;

/*!
 * \class Plugin
 * \brief Base class for plugins
 */
class Plugin : public QWidget
{
protected:
    std::string _name;
    Trace *_trace;
    Render_windowed *_render = nullptr;

public:
    /*!
     * \fn Plugin(QWidget *parent = 0)
     * \brief Default constructor
     */
    Plugin(QWidget *parent = nullptr) :
        QWidget(parent), _trace(nullptr) { }
    /*!
     * \fn init()
     * \brief Initialize the plugin
     */
    virtual void init() = 0;
    /*!
     * \fn clear()
     * \brief Clear the plugin
     */
    virtual void clear() = 0;
    /*!
     * \fn execute()
     * \brief The function executed when we push the execute button from the Plugin window.
     */
    virtual void execute() = 0;
    /*!
     * \fn get_name()
     * \brief Return the name of the plugin
     */
    virtual std::string get_name() = 0;
    /*!
     * \fn set_arguments(std::map<std::string, QVariant *>)
     * \brief Set the arguments of the plugin. Not yet sure for the prototype
     */
    virtual void set_arguments(std::map<std::string /*argname*/, QVariant * /*argValue*/>) = 0;

    /*!
     * \fn set_trace(Trace *t)
     * \brief Set the trace.
     */
    virtual void set_trace(Trace *t) { _trace = t; }

    /*!
     *\brief Render setter
     *\param r Set private _render_windowed to r
     */
    virtual void set_render(Render_windowed *r) { _render = r; }

    /*!
     * \fn new_instance(const std::string &name)
     * \brief Create a static Plugin which corresponds to the name.
     * If the name is unknown, return NULL.
     */
    static Plugin *new_instance(const std::string &name);
};

#endif // PLUGIN_HPP
