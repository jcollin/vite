/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - Bourroux Luca
**
*/
/*!
 *\file Stats_window.hpp
 */

#ifndef _STATISTIC_WINDOW
#define _STATISTIC_WINDOW

/* For moc compilation */
#include <string>
#include <map>
#include <list>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/tree/Interval.hpp"
#include "trace/Trace.hpp"
/* -- */
#include "ui_statistics.h"
#include "plugin/Plugin.hpp"

#include <QtWidgets/qframe.h>
#include <QtWidgets/qscrollarea.h>
#include <QtWidgets/qboxlayout.h>
#include <QtWidgets/qtreewidget.h>
#include <QtCharts/qchart.h>
#include <QChartView>
#if QT_VERSION < 0x060000
#include <QtCharts/chartsnamespace.h> // this one is needed else QChart is not found
#endif
#include "ChartView.hpp"

enum Diagram_Type {
    Horizontal = 0,
    Vertical,
    Counter
};

class Trace;
class Statistics_window : public Plugin, protected Ui::Statistics
{

    Q_OBJECT

private:
    QChart *_qt_chart = nullptr;
    Chart_View *_qt_chart_view = nullptr;
    QFrame *_main_frame = nullptr;
    Trace *_trace = nullptr;

    std::vector<Container *> _selected_containers;

    std::string _file_viewed;

    int _number_of_selected_container = 0;

    /**
     * If we reload automatically the stats when a zoom is done (maybe look when the containers selection will be added if we do not add/remove them)
     * -> a mettre dans le fichier de config
     */
    bool _auto_reload_when_zoom;
    bool _stacked_checked = true;

public:
    Statistics_window(QWidget *parent = nullptr);
    ~Statistics_window() = default;

    /*!
     * \fn set_trace(Trace *trace)
     * \brief Set the trace parsed (give the container names)
     * \param trace The trace.
     */
    void set_trace(Trace *trace) override;

    /*!
     * \fn set_filename(std::string filename)
     * \brief Set the name of the file trace.
     * \param filename The file name.
     */
    void set_filename(std::string filename);

    /** Plugin interface **/
    /*!
     * \fn void set_arguments(std::map<std::string , QVariant *>);
     * \brief Set the arguments of this plugin
     */
    void set_arguments(std::map<std::string /*argname*/, QVariant * /*argValue*/>) override;
    /*!
     * \fn close_window()
     * \brief Properly close the window
     */
    void close_window();
    /*!
     * \fn clear()
     * \brief Clear the tab when switching between tabs
     */
    void clear() override;

    /*!
     * \fn init()
     * \brief Initialize the plugin
     */
    void init() override;

    /*!
     * \fn execute()
     * \brief Execute the plugin. Here it does the same as the reload button
     */
    void execute() override;
    /*!
     * \fn std::string get_name();
     * \brief Return the name of the plugin
     */
    std::string get_name() override;

private:
    /*!
     * \fn set_container_names()
     * \brief Set the names of the containers in the tree
     */
    void set_container_names();
    void set_container_names_rec(QTreeWidgetItem *current_node, Container *current_container);

    /*!
     * \brief Fill the vector of selected containers depending on the ones chosen in the tree widget
     */
    void set_selected_nodes(const std::string &);

public Q_SLOTS:
    void auto_update_stats();

private Q_SLOTS:
    void on_auto_reload_box_stateChanged(int);
    void on_want_stacked_stateChanged(int);

    void on_reload_button_clicked();
    void on_export_button_clicked();
};

#endif
