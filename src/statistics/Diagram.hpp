#pragma once

#include <vector>
#include "ChartCompatibility.hpp"
#include <QtCharts/qchart.h>
class Container;
class Interval;

namespace Diagram {
/*!
 * \fn vertical(QChart& chart, const std::vector<Container*>& containers, const Interval& interval)
 * \brief Will fill chart with a vertical histogram showing the relative time spent on each
 * state type by containers.
 */
extern void vertical(
    QChart &chart, const std::vector<Container *> &containers, const Interval &interval);
/*!
 * \fn vertical(QChart& chart, const std::vector<Container*>& containers, const Interval& interval)
 * \brief Will fill chart with a vertical stacked histogram showing the relative time spent on
 * each state type by containers.
 */
extern void stacked_vertical(
    QChart &chart, const std::vector<Container *> &containers, const Interval &interval);

/*!
 * \fn horizontal(QChart& chart, const std::vector<Container*>& containers, const Interval& interval)
 * \brief Will fill chart with a horizontal histogram showing the relative time spent on each
 * state type by containers.
 */
extern void horizontal(
    QChart &chart, const std::vector<Container *> &containers, const Interval &interval);
/*!
 * \fn horizontal(QChart& chart, const std::vector<Container*>& containers, const Interval& interval)
 * \brief Will fill chart with a horizontal stacked histogram showing the relative time spent on
 * each state type by containers.
 */
extern void stacked_horizontal(
    QChart &chart, const std::vector<Container *> &containers, const Interval &interval);

/*!
 * \fn counter(QChart& chart, const std::vector<Container*>& containers, const Interval& interval)
 * \brief Will fill chart line graph that shows the value of every counter in containers over time.
 */
extern void counter(
    QChart &chart, const std::vector<Container *> &containers, const Interval &interval);
};
