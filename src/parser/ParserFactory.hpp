#ifndef PARSERFACTORY_HPP
#define PARSERFACTORY_HPP

/*!
 * \class ParserFactory
 * \brief Factory which creates a parser depending on the name passed in argument
 */
class ParserFactory
{
private:
    ParserFactory();

public:
    /*!
     * \fn static bool create(Parser **parser, const std::string &filename);
     * \brief Create the parser corresponding to the filename. If no parser is found, the parser Pajé is set as the default one.
     * \param parser The parser we want to fill.
     * \param filename The name of the file we want to open.
     * \return true if the filename as a Parser associed, false in other cases.
     */
    static bool create(Parser **parser, const std::string &filename);
};

#endif /* PARSERFACTORY_HPP */
