/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 * \file TauStructs.hpp
 * \brief This file contains all the structures needed to parse a TAU file.
 */

#ifndef TAUSTRUCTS_HPP
#define TAUSTRUCTS_HPP

namespace Tau {

/*!
 * \struct Container
 * \brief Represents a Tau container
 */
struct Container
{
    unsigned int _node_id;
    unsigned int _thread_id;
    std::string _name;
    bool _is_created;

    /*!
     * \fn Container(unsigned int node_id = 0, unsigned int thread_id = 0, const std::string name = "")
     * \brief Constructor
     * \param node_id The container node id
     * \param thread_id The container thread id
     * \param name The container name
     */
    Container(unsigned int node_id = 0, unsigned int thread_id = 0, const std::string name = "") :
        _node_id(node_id), _thread_id(thread_id), _name(name), _is_created(false) { }
};

/*!
 * \struct State
 * \brief Represents a Tau state
 */
struct State
{
    unsigned int _id;
    unsigned int _state_group;
    std::string _name;
    bool _is_created;

    /*!
     * \fn State(unsigned int id = 0, unsigned int parentGroup = 0, const std::string name = "")
     * \brief Constructor
     * \param id The state id
     * \param parentGroup The parent group id
     * \param name The state name
     */
    State(unsigned int id = 0, unsigned int parentGroup = 0, const std::string name = "") :
        _id(id), _state_group(parentGroup), _name(name), _is_created(false) { }
};

/*!
 * \struct StateGroup
 * \brief Represents a Tau stateType
 */
struct StateGroup
{
    unsigned int _id;
    std::string _name;
    /*!
     * \fn StateGroup(unsigned int id = 0, const std::string name = "")
     * \brief Constructor
     * \param id The state group id
     * \param name The state group name
     */
    StateGroup(unsigned int id = 0, const std::string name = "") :
        _id(id), _name(name) { }
};

/*!
 * \struct Event
 * \brief Represents a Tau event
 */
struct Event
{
    unsigned int _id;
    std::string _name;
    unsigned int _monotonically_increasing;

    /*!
     * \fn Event(unsigned int id = 0, const std::string name = "", unsigned int monotonicallyIncreasing = 0)
     * \brief Constructor
     * \param id The state group id
     * \param name The state group name
     * \param monotonicallyIncreasing unused
     */
    Event(unsigned int id = 0, const std::string name = "", unsigned int monotonicallyIncreasing = 0) :
        _id(id), _name(name), _monotonically_increasing(monotonicallyIncreasing) { }
};
}

#endif // TAUSTRUCTS_HPP
