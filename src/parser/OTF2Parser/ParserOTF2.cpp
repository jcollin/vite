/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/**
 *  @file ParserOTF2.cpp
 *
 *  @author Lagrasse Olivier
 *  @author Johnny Jazeix
 *  @author Mathieu Faverge
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <list>
/* -- */
#include <otf2/otf2.h>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include "parser/Parser.hpp"
#include "parser/OTF2Parser/ParserOTF2.hpp"
#include "parser/OTF2Parser/ParserDefinitionOTF2.hpp"
#include "parser/OTF2Parser/ParserEventOTF2.hpp"
/* -- */

using namespace std;

#define VITE_OTF_2_MAXFILES_OPEN 100

#define VITE_ERR_OTF_2_OPENREADER "Failed to create the OTF2 Reader\n"

ParserOTF2::ParserOTF2() { }
ParserOTF2::ParserOTF2(const string &filename) :
    Parser(filename) { }
ParserOTF2::~ParserOTF2() { }

void ParserOTF2::parse(Trace &trace,
                       bool finish_trace_after_parse) {

    ParserDefinitionOTF2 *parserdefinition;
    ParserEventOTF2 *parserevent;
    OTF2_Reader *reader;
    string filename = get_next_file_to_parse();

    reader = OTF2_Reader_Open(filename.c_str());
    if (reader == NULL) {
        cerr << QObject::tr(VITE_ERR_OTF_2_OPENREADER).toStdString() << endl;
        finish();

        if (finish_trace_after_parse) { // true by default
            trace.finish();
        }
        return;
    }

    parserdefinition = new ParserDefinitionOTF2(reader);
    parserdefinition->set_handlers(&trace);
    parserdefinition->read_definitions(reader);
    parserdefinition->create_container_types(&trace);
    parserdefinition->initialize_types(&trace);

#if DEBUG
    parserdefinition->print_definitions();
#endif // DEBUG

    parserevent = new ParserEventOTF2(reader);
    parserevent->set_handlers(reader, &trace);

    parserevent->read_events(reader);

    // parserevent->read_markers(reader);

    finish();

    if (finish_trace_after_parse) { // true by default
        trace.finish();
    }

    delete parserevent;
    delete parserdefinition;

    OTF2_Reader_Close(reader);
}

float ParserOTF2::get_percent_loaded() const {
    // return ParserEventOTF::get_percent_loaded();
    return 1;
}
