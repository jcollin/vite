/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/**
 *  @file ParserEventOTF2.hpp
 *
 *  @brief This file contains the event parser used by the ParserOTF2.
 *
 *  @author Lagrasse Olivier
 *  @author Johnny Jazeix
 *  @author Mathieu Faverge
 *
 */
#ifndef PARSEREVENTOTF2_HPP
#define PARSEREVENTOTF2_HPP

class Trace;
class ParserDefinitionOTF2;

/**
 * \class ParserEventOTF2
 * \brief Reads Use handlers in order to fill the data structure
 */

class ParserEventOTF2
{
private:
    /*!
     * Reader for the file
     */
    OTF2_GlobalEvtReader *_global_evt_reader;

    OTF2_GlobalEvtReaderCallbacks *_global_evt_callbacks;

    static std::map<const String, Container *, String::less_than> _containers;

    ParserEventOTF2(const ParserEventOTF2 &);

    static OTF2_CallbackCode callback_Unknown(OTF2_LocationRef locationID,
                                              OTF2_TimeStamp time,
                                              void *userData,
                                              OTF2_AttributeList *attributeList);

    static OTF2_CallbackCode callback_BufferFlush(OTF2_LocationRef locationID,
                                                  OTF2_TimeStamp time,
                                                  void *userData,
                                                  OTF2_AttributeList *attributeList,
                                                  OTF2_TimeStamp stopTime);

    static OTF2_CallbackCode callback_MeasurementOnOff(OTF2_LocationRef locationID,
                                                       OTF2_TimeStamp time,
                                                       void *userData,
                                                       OTF2_AttributeList *attributeList,
                                                       OTF2_MeasurementMode measurementMode);

    static OTF2_CallbackCode callback_Enter(OTF2_LocationRef locationID,
                                            OTF2_TimeStamp time,
                                            void *userData,
                                            OTF2_AttributeList *attributeList,
                                            OTF2_RegionRef region);

    static OTF2_CallbackCode callback_Leave(OTF2_LocationRef locationID,
                                            OTF2_TimeStamp time,
                                            void *userData,
                                            OTF2_AttributeList *attributeList,
                                            OTF2_RegionRef region);

    static OTF2_CallbackCode callback_MpiSend(OTF2_LocationRef locationID,
                                              OTF2_TimeStamp time,
                                              void *userData,
                                              OTF2_AttributeList *attributeList,
                                              uint32_t receiver,
                                              OTF2_CommRef communicator,
                                              uint32_t msgTag,
                                              uint64_t msgLength);

    static OTF2_CallbackCode callback_MpiIsend(OTF2_LocationRef locationID,
                                               OTF2_TimeStamp time,
                                               void *userData,
                                               OTF2_AttributeList *attributeList,
                                               uint32_t receiver,
                                               OTF2_CommRef communicator,
                                               uint32_t msgTag,
                                               uint64_t msgLength,
                                               uint64_t requestID);

    static OTF2_CallbackCode callback_MpiIsendComplete(OTF2_LocationRef locationID,
                                                       OTF2_TimeStamp time,
                                                       void *userData,
                                                       OTF2_AttributeList *attributeList,
                                                       uint64_t requestID);

    static OTF2_CallbackCode callback_MpiIrecvRequest(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void *userData,
                                                      OTF2_AttributeList *attributeList,
                                                      uint64_t requestID);

    static OTF2_CallbackCode callback_MpiRecv(OTF2_LocationRef locationID,
                                              OTF2_TimeStamp time,
                                              void *userData,
                                              OTF2_AttributeList *attributeList,
                                              uint32_t sender,
                                              OTF2_CommRef communicator,
                                              uint32_t msgTag,
                                              uint64_t msgLength);

    static OTF2_CallbackCode callback_MpiIrecv(OTF2_LocationRef locationID,
                                               OTF2_TimeStamp time,
                                               void *userData,
                                               OTF2_AttributeList *attributeList,
                                               uint32_t sender,
                                               OTF2_CommRef communicator,
                                               uint32_t msgTag,
                                               uint64_t msgLength,
                                               uint64_t requestID);

    static OTF2_CallbackCode callback_MpiRequestTest(OTF2_LocationRef locationID,
                                                     OTF2_TimeStamp time,
                                                     void *userData,
                                                     OTF2_AttributeList *attributeList,
                                                     uint64_t requestID);

    static OTF2_CallbackCode callback_MpiRequestCancelled(OTF2_LocationRef locationID,
                                                          OTF2_TimeStamp time,
                                                          void *userData,
                                                          OTF2_AttributeList *attributeList,
                                                          uint64_t requestID);

    static OTF2_CallbackCode callback_MpiCollectiveBegin(OTF2_LocationRef locationID,
                                                         OTF2_TimeStamp time,
                                                         void *userData,
                                                         OTF2_AttributeList *attributeList);

    static OTF2_CallbackCode callback_MpiCollectiveEnd(OTF2_LocationRef locationID,
                                                       OTF2_TimeStamp time,
                                                       void *userData,
                                                       OTF2_AttributeList *attributeList,
                                                       OTF2_CollectiveOp collectiveOp,
                                                       OTF2_CommRef communicator,
                                                       uint32_t root,
                                                       uint64_t sizeSent,
                                                       uint64_t sizeReceived);

    static OTF2_CallbackCode callback_OmpFork(OTF2_LocationRef locationID,
                                              OTF2_TimeStamp time,
                                              void *userData,
                                              OTF2_AttributeList *attributeList,
                                              uint32_t numberOfRequestedThreads);

    static OTF2_CallbackCode callback_OmpJoin(OTF2_LocationRef locationID,
                                              OTF2_TimeStamp time,
                                              void *userData,
                                              OTF2_AttributeList *attributeList);

    static OTF2_CallbackCode callback_OmpAcquireLock(OTF2_LocationRef locationID,
                                                     OTF2_TimeStamp time,
                                                     void *userData,
                                                     OTF2_AttributeList *attributeList,
                                                     uint32_t lockID,
                                                     uint32_t acquisitionOrder);

    static OTF2_CallbackCode callback_OmpReleaseLock(OTF2_LocationRef locationID,
                                                     OTF2_TimeStamp time,
                                                     void *userData,
                                                     OTF2_AttributeList *attributeList,
                                                     uint32_t lockID,
                                                     uint32_t acquisitionOrder);

    static OTF2_CallbackCode callback_OmpTaskCreate(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void *userData,
                                                    OTF2_AttributeList *attributeList,
                                                    uint64_t taskID);

    static OTF2_CallbackCode callback_OmpTaskSwitch(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void *userData,
                                                    OTF2_AttributeList *attributeList,
                                                    uint64_t taskID);

    static OTF2_CallbackCode callback_OmpTaskComplete(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void *userData,
                                                      OTF2_AttributeList *attributeList,
                                                      uint64_t taskID);

    static OTF2_CallbackCode callback_Metric(OTF2_LocationRef locationID,
                                             OTF2_TimeStamp time,
                                             void *userData,
                                             OTF2_AttributeList *attributeList,
                                             OTF2_MetricRef metric,
                                             uint8_t numberOfMetrics,
                                             const OTF2_Type *typeIDs,
                                             const OTF2_MetricValue *metricValues);

    static OTF2_CallbackCode callback_ParameterString(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void *userData,
                                                      OTF2_AttributeList *attributeList,
                                                      OTF2_ParameterRef parameter,
                                                      OTF2_StringRef string);

    static OTF2_CallbackCode callback_ParameterInt(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void *userData,
                                                   OTF2_AttributeList *attributeList,
                                                   OTF2_ParameterRef parameter,
                                                   int64_t value);

    static OTF2_CallbackCode callback_ParameterUnsignedInt(OTF2_LocationRef locationID,
                                                           OTF2_TimeStamp time,
                                                           void *userData,
                                                           OTF2_AttributeList *attributeList,
                                                           OTF2_ParameterRef parameter,
                                                           uint64_t value);

    static OTF2_CallbackCode callback_RmaWinCreate(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void *userData,
                                                   OTF2_AttributeList *attributeList,
                                                   OTF2_RmaWinRef win);

    static OTF2_CallbackCode callback_RmaWinDestroy(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void *userData,
                                                    OTF2_AttributeList *attributeList,
                                                    OTF2_RmaWinRef win);

    static OTF2_CallbackCode callback_RmaCollectiveBegin(OTF2_LocationRef locationID,
                                                         OTF2_TimeStamp time,
                                                         void *userData,
                                                         OTF2_AttributeList *attributeList);

    static OTF2_CallbackCode callback_RmaCollectiveEnd(OTF2_LocationRef locationID,
                                                       OTF2_TimeStamp time,
                                                       void *userData,
                                                       OTF2_AttributeList *attributeList,
                                                       OTF2_CollectiveOp collectiveOp,
                                                       OTF2_RmaSyncLevel syncLevel,
                                                       OTF2_RmaWinRef win,
                                                       uint32_t root,
                                                       uint64_t bytesSent,
                                                       uint64_t bytesReceived);

    static OTF2_CallbackCode callback_RmaGroupSync(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void *userData,
                                                   OTF2_AttributeList *attributeList,
                                                   OTF2_RmaSyncLevel syncLevel,
                                                   OTF2_RmaWinRef win,
                                                   OTF2_GroupRef group);

    static OTF2_CallbackCode callback_RmaRequestLock(OTF2_LocationRef locationID,
                                                     OTF2_TimeStamp time,
                                                     void *userData,
                                                     OTF2_AttributeList *attributeList,
                                                     OTF2_RmaWinRef win,
                                                     uint32_t remote,
                                                     uint64_t lockId,
                                                     OTF2_LockType lockType);

    static OTF2_CallbackCode callback_RmaAcquireLock(OTF2_LocationRef locationID,
                                                     OTF2_TimeStamp time,
                                                     void *userData,
                                                     OTF2_AttributeList *attributeList,
                                                     OTF2_RmaWinRef win,
                                                     uint32_t remote,
                                                     uint64_t lockId,
                                                     OTF2_LockType lockType);

    static OTF2_CallbackCode callback_RmaTryLock(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void *userData,
                                                 OTF2_AttributeList *attributeList,
                                                 OTF2_RmaWinRef win,
                                                 uint32_t remote,
                                                 uint64_t lockId,
                                                 OTF2_LockType lockType);

    static OTF2_CallbackCode callback_RmaReleaseLock(OTF2_LocationRef locationID,
                                                     OTF2_TimeStamp time,
                                                     void *userData,
                                                     OTF2_AttributeList *attributeList,
                                                     OTF2_RmaWinRef win,
                                                     uint32_t remote,
                                                     uint64_t lockId);

    static OTF2_CallbackCode callback_RmaSync(OTF2_LocationRef locationID,
                                              OTF2_TimeStamp time,
                                              void *userData,
                                              OTF2_AttributeList *attributeList,
                                              OTF2_RmaWinRef win,
                                              uint32_t remote,
                                              OTF2_RmaSyncType syncType);

    static OTF2_CallbackCode callback_RmaWaitChange(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void *userData,
                                                    OTF2_AttributeList *attributeList,
                                                    OTF2_RmaWinRef win);

    static OTF2_CallbackCode callback_RmaPut(OTF2_LocationRef locationID,
                                             OTF2_TimeStamp time,
                                             void *userData,
                                             OTF2_AttributeList *attributeList,
                                             OTF2_RmaWinRef win,
                                             uint32_t remote,
                                             uint64_t bytes,
                                             uint64_t matchingId);

    static OTF2_CallbackCode callback_RmaGet(OTF2_LocationRef locationID,
                                             OTF2_TimeStamp time,
                                             void *userData,
                                             OTF2_AttributeList *attributeList,
                                             OTF2_RmaWinRef win,
                                             uint32_t remote,
                                             uint64_t bytes,
                                             uint64_t matchingId);

    static OTF2_CallbackCode callback_RmaAtomic(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void *userData,
                                                OTF2_AttributeList *attributeList,
                                                OTF2_RmaWinRef win,
                                                uint32_t remote,
                                                OTF2_RmaAtomicType type,
                                                uint64_t bytesSent,
                                                uint64_t bytesReceived,
                                                uint64_t matchingId);

    static OTF2_CallbackCode callback_RmaOpCompleteBlocking(OTF2_LocationRef locationID,
                                                            OTF2_TimeStamp time,
                                                            void *userData,
                                                            OTF2_AttributeList *attributeList,
                                                            OTF2_RmaWinRef win,
                                                            uint64_t matchingId);

    static OTF2_CallbackCode callback_RmaOpCompleteNonBlocking(OTF2_LocationRef locationID,
                                                               OTF2_TimeStamp time,
                                                               void *userData,
                                                               OTF2_AttributeList *attributeList,
                                                               OTF2_RmaWinRef win,
                                                               uint64_t matchingId);

    static OTF2_CallbackCode callback_RmaOpTest(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void *userData,
                                                OTF2_AttributeList *attributeList,
                                                OTF2_RmaWinRef win,
                                                uint64_t matchingId);

    static OTF2_CallbackCode callback_RmaOpCompleteRemote(OTF2_LocationRef locationID,
                                                          OTF2_TimeStamp time,
                                                          void *userData,
                                                          OTF2_AttributeList *attributeList,
                                                          OTF2_RmaWinRef win,
                                                          uint64_t matchingId);

    static OTF2_CallbackCode callback_ThreadFork(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void *userData,
                                                 OTF2_AttributeList *attributeList,
                                                 OTF2_Paradigm model,
                                                 uint32_t numberOfRequestedThreads);

    static OTF2_CallbackCode callback_ThreadJoin(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void *userData,
                                                 OTF2_AttributeList *attributeList,
                                                 OTF2_Paradigm model);

    static OTF2_CallbackCode callback_ThreadTeamBegin(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void *userData,
                                                      OTF2_AttributeList *attributeList,
                                                      OTF2_CommRef threadTeam);

    static OTF2_CallbackCode callback_ThreadTeamEnd(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void *userData,
                                                    OTF2_AttributeList *attributeList,
                                                    OTF2_CommRef threadTeam);

    static OTF2_CallbackCode callback_ThreadAcquireLock(OTF2_LocationRef locationID,
                                                        OTF2_TimeStamp time,
                                                        void *userData,
                                                        OTF2_AttributeList *attributeList,
                                                        OTF2_Paradigm model,
                                                        uint32_t lockID,
                                                        uint32_t acquisitionOrder);

    static OTF2_CallbackCode callback_ThreadReleaseLock(OTF2_LocationRef locationID,
                                                        OTF2_TimeStamp time,
                                                        void *userData,
                                                        OTF2_AttributeList *attributeList,
                                                        OTF2_Paradigm model,
                                                        uint32_t lockID,
                                                        uint32_t acquisitionOrder);

    static OTF2_CallbackCode callback_ThreadTaskCreate(OTF2_LocationRef locationID,
                                                       OTF2_TimeStamp time,
                                                       void *userData,
                                                       OTF2_AttributeList *attributeList,
                                                       OTF2_CommRef threadTeam,
                                                       uint32_t creatingThread,
                                                       uint32_t generationNumber);

    static OTF2_CallbackCode callback_ThreadTaskSwitch(OTF2_LocationRef locationID,
                                                       OTF2_TimeStamp time,
                                                       void *userData,
                                                       OTF2_AttributeList *attributeList,
                                                       OTF2_CommRef threadTeam,
                                                       uint32_t creatingThread,
                                                       uint32_t generationNumber);

    static OTF2_CallbackCode callback_ThreadTaskComplete(OTF2_LocationRef locationID,
                                                         OTF2_TimeStamp time,
                                                         void *userData,
                                                         OTF2_AttributeList *attributeList,
                                                         OTF2_CommRef threadTeam,
                                                         uint32_t creatingThread,
                                                         uint32_t generationNumber);

    static OTF2_CallbackCode callback_ThreadCreate(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void *userData,
                                                   OTF2_AttributeList *attributeList,
                                                   OTF2_CommRef threadContingent,
                                                   uint64_t sequenceCount);

    static OTF2_CallbackCode callback_ThreadBegin(OTF2_LocationRef locationID,
                                                  OTF2_TimeStamp time,
                                                  void *userData,
                                                  OTF2_AttributeList *attributeList,
                                                  OTF2_CommRef threadContingent,
                                                  uint64_t sequenceCount);

    static OTF2_CallbackCode callback_ThreadWait(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void *userData,
                                                 OTF2_AttributeList *attributeList,
                                                 OTF2_CommRef threadContingent,
                                                 uint64_t sequenceCount);

    static OTF2_CallbackCode callback_ThreadEnd(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void *userData,
                                                OTF2_AttributeList *attributeList,
                                                OTF2_CommRef threadContingent,
                                                uint64_t sequenceCount);

    static OTF2_CallbackCode callback_CallingContextEnter(OTF2_LocationRef locationID,
                                                          OTF2_TimeStamp time,
                                                          void *userData,
                                                          OTF2_AttributeList *attributeList,
                                                          OTF2_CallingContextRef callingContext,
                                                          uint32_t unwindDistance);

    static OTF2_CallbackCode callback_CallingContextLeave(OTF2_LocationRef locationID,
                                                          OTF2_TimeStamp time,
                                                          void *userData,
                                                          OTF2_AttributeList *attributeList,
                                                          OTF2_CallingContextRef callingContext);

    static OTF2_CallbackCode callback_CallingContextSample(OTF2_LocationRef locationID,
                                                           OTF2_TimeStamp time,
                                                           void *userData,
                                                           OTF2_AttributeList *attributeList,
                                                           OTF2_CallingContextRef callingContext,
                                                           uint32_t unwindDistance,
                                                           OTF2_InterruptGeneratorRef interruptGenerator);

    static OTF2_CallbackCode callback_IoCreateHandle(OTF2_LocationRef locationID,
                                                     OTF2_TimeStamp time,
                                                     void *userData,
                                                     OTF2_AttributeList *attributeList,
                                                     OTF2_IoHandleRef handle,
                                                     OTF2_IoAccessMode mode,
                                                     OTF2_IoCreationFlag creationFlags,
                                                     OTF2_IoStatusFlag statusFlags);

    static OTF2_CallbackCode callback_IoDestroyHandle(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void *userData,
                                                      OTF2_AttributeList *attributeList,
                                                      OTF2_IoHandleRef handle);

    static OTF2_CallbackCode callback_IoDuplicateHandle(OTF2_LocationRef locationID,
                                                        OTF2_TimeStamp time,
                                                        void *userData,
                                                        OTF2_AttributeList *attributeList,
                                                        OTF2_IoHandleRef oldHandle,
                                                        OTF2_IoHandleRef newHandle,
                                                        OTF2_IoStatusFlag statusFlags);

    static OTF2_CallbackCode callback_IoSeek(OTF2_LocationRef locationID,
                                             OTF2_TimeStamp time,
                                             void *userData,
                                             OTF2_AttributeList *attributeList,
                                             OTF2_IoHandleRef handle,
                                             int64_t offsetRequest,
                                             OTF2_IoSeekOption whence,
                                             uint64_t offsetResult);

    static OTF2_CallbackCode callback_IoChangeStatusFlags(OTF2_LocationRef locationID,
                                                          OTF2_TimeStamp time,
                                                          void *userData,
                                                          OTF2_AttributeList *attributeList,
                                                          OTF2_IoHandleRef handle,
                                                          OTF2_IoStatusFlag statusFlags);

    static OTF2_CallbackCode callback_IoDeleteFile(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void *userData,
                                                   OTF2_AttributeList *attributeList,
                                                   OTF2_IoParadigmRef ioParadigm,
                                                   OTF2_IoFileRef file);

    static OTF2_CallbackCode callback_IoOperationBegin(OTF2_LocationRef locationID,
                                                       OTF2_TimeStamp time,
                                                       void *userData,
                                                       OTF2_AttributeList *attributeList,
                                                       OTF2_IoHandleRef handle,
                                                       OTF2_IoOperationMode mode,
                                                       OTF2_IoOperationFlag operationFlags,
                                                       uint64_t bytesRequest,
                                                       uint64_t matchingId);

    static OTF2_CallbackCode callback_IoOperationTest(OTF2_LocationRef locationID,
                                                      OTF2_TimeStamp time,
                                                      void *userData,
                                                      OTF2_AttributeList *attributeList,
                                                      OTF2_IoHandleRef handle,
                                                      uint64_t matchingId);

    static OTF2_CallbackCode callback_IoOperationIssued(OTF2_LocationRef locationID,
                                                        OTF2_TimeStamp time,
                                                        void *userData,
                                                        OTF2_AttributeList *attributeList,
                                                        OTF2_IoHandleRef handle,
                                                        uint64_t matchingId);

    static OTF2_CallbackCode callback_IoOperationComplete(OTF2_LocationRef locationID,
                                                          OTF2_TimeStamp time,
                                                          void *userData,
                                                          OTF2_AttributeList *attributeList,
                                                          OTF2_IoHandleRef handle,
                                                          uint64_t bytesResult,
                                                          uint64_t matchingId);

    static OTF2_CallbackCode callback_IoOperationCancelled(OTF2_LocationRef locationID,
                                                           OTF2_TimeStamp time,
                                                           void *userData,
                                                           OTF2_AttributeList *attributeList,
                                                           OTF2_IoHandleRef handle,
                                                           uint64_t matchingId);

    static OTF2_CallbackCode callback_IoAcquireLock(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void *userData,
                                                    OTF2_AttributeList *attributeList,
                                                    OTF2_IoHandleRef handle,
                                                    OTF2_LockType lockType);

    static OTF2_CallbackCode callback_IoReleaseLock(OTF2_LocationRef locationID,
                                                    OTF2_TimeStamp time,
                                                    void *userData,
                                                    OTF2_AttributeList *attributeList,
                                                    OTF2_IoHandleRef handle,
                                                    OTF2_LockType lockType);

    static OTF2_CallbackCode callback_IoTryLock(OTF2_LocationRef locationID,
                                                OTF2_TimeStamp time,
                                                void *userData,
                                                OTF2_AttributeList *attributeList,
                                                OTF2_IoHandleRef handle,
                                                OTF2_LockType lockType);

    static OTF2_CallbackCode callback_ProgramBegin(OTF2_LocationRef locationID,
                                                   OTF2_TimeStamp time,
                                                   void *userData,
                                                   OTF2_AttributeList *attributeList,
                                                   OTF2_StringRef programName,
                                                   uint32_t numberOfArguments,
                                                   const OTF2_StringRef *programArguments);

    static OTF2_CallbackCode callback_ProgramEnd(OTF2_LocationRef locationID,
                                                 OTF2_TimeStamp time,
                                                 void *userData,
                                                 OTF2_AttributeList *attributeList,
                                                 int64_t exitStatus);

public:
    /*!
     * \fn ParserEventOTF2()
     * \brief constructor
     */
    ParserEventOTF2(OTF2_Reader *reader);

    /*!
     * \fn ~ParserEventOTF2()
     * \brief constructor
     */
    ~ParserEventOTF2();

    /*!
     * \fn set_handlers(OTF2_Reader *reader, Trace *t)
     * \brief Create and set the handlers for the event parsing.
     * \param t The trace we want to store in.
     */
    void set_handlers(OTF2_Reader *reader, Trace *t);

    /*!
     * \fn read_events(OTF_Reader2 *reader)
     * \brief Begin the reading of the events
     * \param reader The main otf file we want to read in.
     */
    int read_events(OTF2_Reader *reader);

    /*!
     * \fn get_percent_loaded()
     * \brief get the size already parsed.
     * It is an approximation, we consider that parsing the definitions is done in a constant time.
     * \return the scale of the size already parsed. (between 0 and 1)
     */
    static float get_percent_loaded();
};
#endif // PARSEREVENTOTF2_HPP
