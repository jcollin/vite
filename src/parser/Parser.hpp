/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 *\file Parser.hpp
 *\brief This file contains the abstract layer for the parser.
 */

#ifndef PARSER_HPP
#define PARSER_HPP

#include <queue>
#include <string>

class Trace;
#include <QObject>
/*!
 * \class Parser Parser.hpp "../parser/src/Parser.hpp"
 * \brief Contains the definition of the parser interface.
 */

class Parser : public QObject
{
    Q_OBJECT

protected:
    bool _is_finished;
    bool _is_canceled;

    // list of the files to parse
    std::queue<std::string> _q;

    /*!
     *  \fn const std::string get_next_file_to_parse()
     *  \brief return the name of the next file to parse.
     */
    const std::string get_next_file_to_parse();

public:
    Parser();
    Parser(const std::string &filename);
    ~Parser() override = default;

    /*!
     *  \fn parse(const std::string &filename, Trace &trace, bool finish_trace_after_parse = true)
     *  \param filename the name of the file to parse
     *  \param trace the structure of data to fill
     *  \param finish_trace_after_parse boolean set if we do not have to finish the trace after parsing
     */
    void parse(const std::string &filename, Trace &trace, bool finish_trace_after_parse = true);

    /*!
     *  \fn parse(Trace &trace, bool finish_trace_after_parse = true) = 0
     *  \param trace the structure of data to fill
     *  \param finish_trace_after_parse boolean set if we do not have to finish the trace after parsing
     */
    virtual void parse(Trace &trace, bool finish_trace_after_parse = true) = 0;

    /*!
     *  \fn get_percent_loaded() const = 0;
     *  \return the scale of the size already loaded of the file by the parser. (between 0 and 1)
     */
    virtual float get_percent_loaded() const = 0;

    /*!
     *  \fn is_end_of_parsing() const
     *  \brief true is the parsing is finished
     *  \return boolean
     */
    virtual bool is_end_of_parsing() const;

    /*!
     * \fn set_canceled()
     * \brief called when the loading of the trace is canceled by the user
     */
    virtual void set_canceled();

    /*!
     *  \fn set_file_to_parse(const std::string &filename)
     *  \param filename : the name of the file to parse
     *  \brief set the name of the file to parse.
     */
    virtual void set_file_to_parse(const std::string &filename);

    /*!
     *  \fn finish()
     *  \brief set the end of the parsing
     */
    virtual void finish();
};

#endif // PARSER_HPP
