/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 *\file ParserEventPaje.hpp
 *\brief This file contains the event Paje used by the ParserPaje.
 */

#ifndef MT_PARSEREVENTPAJE_HPP
#define MT_PARSEREVENTPAJE_HPP

/**
 * \class mt_ParserEventPaje
 * \brief Reads Hash Table to find fill the Str -> multithread version
 *
 */

#include <QObject>
#include <QThread>
#include <string>
#include <map>
#include <set>
#include <queue>
#include <list>
/* -- */
#include "common/Errors.hpp"
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include <fstream>
#include "parser/PajeParser/mt_PajeFileManager.hpp"
#include "parser/PajeParser/PajeDefinition.hpp"
#include "parser/PajeParser/ParserDefinitionPaje.hpp"
#include "trace/TraceBuilderThread.hpp"

class Container;
class ParserDefinitionPaje;

typedef std::map<const String, Container *, String::less_than> Container_map;

class mt_ParserEventPaje : public QObject
{
    Q_OBJECT
private:
    ParserDefinitionPaje *_Definitions;

    Container_map _containers;

    mt_ParserEventPaje(const mt_ParserEventPaje &);

    Trace *_trace;

public:
    mt_ParserEventPaje(ParserDefinitionPaje *Defs);

    ~mt_ParserEventPaje() override;

    void setTrace(Trace *trace);

    /*!
     *  \fn store_event(const PajeLine_t *line, Trace &trace)
     *  \param line the line containing the event.
     *  \param trace where we store the event.
     */
    int store_event(const PajeLine_t *line, Trace &trace, Trace_builder_struct *tb_struct);

Q_SIGNALS:
    void build_trace(int n, Trace_builder_struct *tb_struct);
};

#endif // PARSEREVENTPAJE_HPP
