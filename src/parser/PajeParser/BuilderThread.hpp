/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef BUILDING_THREAD_HPP
#define BUILDING_THREAD_HPP

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QSemaphore>
#include <queue>
#include <parser/PajeParser/mt_PajeFileManager.hpp>

class Trace;
class Parser;
class mt_ParserEventPaje;
struct Trace_builder_struct;
/*!
 * \class ParsingThread
 * \brief Thread to parse asynchronously a trace with any parser available
 */
class BuilderThread : public QObject
{
    Q_OBJECT

private:
    mt_ParserEventPaje *_event_parser;
    Parser *_parser;
    std::queue<PajeLine *> *_q;
    QWaitCondition *_cond;
    QWaitCondition *_trace_cond;
    QMutex *_mutex;
    QMutex *_mutex2;
    QSemaphore *_freeSlots;
    QSemaphore *_linesProduced;

    Trace *_trace;
    bool _is_finished;

public:
    /*!
     *  \fn ParsingThread(Parser *p, Trace *t)
     *  \param p the event parser used to parse the file.
     *  \param t the trace where we store data.
     *  \param parser the parser used to parse the file.
     */
    BuilderThread(mt_ParserEventPaje *p, Trace *trace, QWaitCondition *cond,
                  QWaitCondition *trace_cond, QSemaphore *freeBytes,
                  QSemaphore *linesProduced, QMutex *mutex, QMutex *mutex2,
                  Parser *parser);

    bool is_finished();

public Q_SLOTS:
    void finish_build();

    /*!
     *  \fn run()
     *  \brief run the thread.
     */
    void run(unsigned int n, PajeLine *line);

Q_SIGNALS:
    void build_finished();
    void build_trace(int, Trace_builder_struct *);
};
#endif // PARSING_THREAD_HPP
