#ifndef VALUES_PARSER_MATRIX_VISUALIZER_HPP
#define VALUES_PARSER_MATRIX_VISUALIZER_HPP

#include "../Formats/SymbolMatrix.hpp"
#include "Parser.hpp"

class ValuesParser : public Parser<symbol_matrix_t>
{
public:
    ValuesParser();

    symbol_matrix_t *parse(std::string path, symbol_matrix_t *m);
    float get_percent_loaded() const;

    bool is_finished() const;
    bool is_cancelled() const;

    void set_canceled();
    void finish();

private:
    float m_percentage_loaded;
};

#endif
