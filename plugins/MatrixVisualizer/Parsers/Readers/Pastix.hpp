#ifndef READER_PASTIX_HPP
#define READER_PASTIX_HPP

#include "../../Formats/SymbolMatrix.hpp"
#include <cstdio>

// Functionnal
int pastix_read_int(FILE *stream, int32_t *value);

// Reader
int pastix_read_symbol(FILE *stream, symbol_matrix_t *matrix);

#endif
