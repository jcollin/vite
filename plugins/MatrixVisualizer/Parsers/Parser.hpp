#ifndef PARSER_MATRIX_VISUALIZER_HPP
#define PARSER_MATRIX_VISUALIZER_HPP

#include <string>

template <typename T>
class Parser
{
public:
    Parser() { }

    virtual T *parse(std::string path, T *m) = 0;

    virtual float get_percent_loaded() const = 0;
    virtual bool is_finished() const = 0;
    virtual bool is_cancelled() const = 0;

    virtual void set_canceled() = 0;
    virtual void finish() = 0;

protected:
    bool m_is_finished = false;
    bool m_is_canceled = false;
};

#endif
