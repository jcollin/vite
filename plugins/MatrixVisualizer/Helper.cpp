#include "Helper.hpp"

#include <cstdarg>
#include <iostream>

#include "MatrixVisualizer.hpp"

namespace Helper {
void log(LogStatus status, const char *format, ...) {
    va_list ap;
    va_start(ap, format);

    Matrix_visualizer::get_instance()->log(status, format, ap);

    va_end(ap);
}

void set_infos(const char *format, ...) {
    va_list ap;
    va_start(ap, format);

    Matrix_visualizer::get_instance()->set_infos(format, ap);

    va_end(ap);
}
}
