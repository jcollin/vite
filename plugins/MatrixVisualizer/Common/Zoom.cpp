#include "Zoom.hpp"

Zoom::Zoom(symbol_matrix_t *matrix) :
    m_matrix(matrix) { }

Zoom::~Zoom() { }

GLfloat Zoom::getColor(int x, int y) const {
    return m_colors[x][y];
}