#ifndef MATRIX_GL_WIDGET_HPP
#define MATRIX_GL_WIDGET_HPP

#include "Configuration.hpp"

#include <QLabel>
#include <QElapsedTimer>

#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QKeyEvent>

#include <stack>

class Zoom;

class MatrixGLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    MatrixGLWidget(QWidget *parent, symbol_matrix_t *matrix, QLabel *label, bool quadtree_checked);
    ~MatrixGLWidget();

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void keyPressEvent(QKeyEvent *keyEvent) override;

    void mousePressEvent(QMouseEvent *mouseEvent) override;
    void mouseReleaseEvent(QMouseEvent *mouseEvent) override;
    void mouseMoveEvent(QMouseEvent *mouseEvent) override;

private:
    typedef struct CameraPosition
    {
        double m_cameraX;
        double m_cameraY;
        double m_cameraDx;
        double m_cameraDy;
    } CameraPosition;

private:
    void refreshCamera();

private:
    uint32_t m_frameCount;
    QElapsedTimer m_time;
    QLabel *m_label;

    char m_fpsString[256] = { '\0' };

    double m_qtToGLWidthCoeff;
    double m_qtToGLHeightCoeff;

    // Zoom
    Zoom *m_zoom;
    int m_mouseXClicked;
    int m_mouseYClicked;
    CameraPosition m_camera;

    int m_drawTempSelection = 0;
    int m_tempSelectionX = 0;
    int m_tempSelectionY = 0;
    int m_tempSelectionDx = 0;
    int m_tempSelectionDy = 0;

    // Zoom stack
    std::stack<CameraPosition> m_savedPositions;
};

#endif
